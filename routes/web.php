<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::prefix('/news')->group(function () {
    Route::name('posts.')->group(function() {
        Route::get('', ['as'=>'list', 'uses' => 'PostController@index']);
        Route::middleware(['auth'])->group(function () {
            Route::post('', ['as'=>'store', 'uses' => 'PostController@store']);
            Route::get('/create', ['as'=>'create', 'uses' => 'PostController@create']);
            Route::get('/{post}/delete', ['as'=>'delete', 'uses' => 'PostController@destroy']);
            Route::get('/{post}/edit', ['as'=>'edit', 'uses' => 'PostController@edit']);
            Route::post('/{post}', ['as'=>'update', 'uses' => 'PostController@update']);
        });
        Route::get('/{post}', ['as'=>'show', 'uses' => 'PostController@show']);
    });
});