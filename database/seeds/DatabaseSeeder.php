<?php

use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();
        factory(\App\PostCategory::class, rand(5,7))
            ->create();
        $allPostCategories = \App\PostCategory::all();
        factory(\App\User::class, 1)
            ->create()
            ->each(function (\App\User $u) use($output, $allPostCategories) {
                $u->save();
                factory(\App\Post::class, 12)
                    ->create(['user_id' => $u->id])
                    ->each(function (\App\Post $p) use ($allPostCategories) {
                        $allPostCategories->random(rand(2,4))
                            ->each(function (\App\PostCategory $pc) use ($p) {
                               $p->categories()->attach($pc);
                            });
                    });
            });
    }
}
