@extends('layouts.app')

@section('content')
    @auth
    <div class="row">
        <div class="col-md-12">
            <a href="{{route('posts.delete', ['id' => $post->id])}}" class="btn btn-danger">Delete this post</a>
            <a href="{{route('posts.edit', ['id' => $post->id])}}" class="btn btn-primary">Edit this post</a>
        </div>
    </div>
    @endauth
    <div class="row">
        <div class="col-md-12">
            <h3>{{$post->title}}</h3>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p>
                @foreach($post->categories as $category)
                    <a href="{{route('posts.list')}}?category={{$category->id}}" class="badge">{{$category->name}}</a>
                @endforeach
            </p>
        </div>
    </div>
    <hr style="border-color: #9c9c9c;">
    @if($post->image)
        <div class="row">
            <div class="col-md-12">
                <img src="{{$post->image_url}}" alt="Post image" style="max-width: 500px;">
            </div>
        </div>
    @endif
    <div class="row" style="padding-top: 50px">
        <div class="col-md-12">
            <p style="font-size: 18px">
                {!!nl2br(htmlspecialchars($post->content))!!}
            </p>
        </div>
    </div>
@endsection