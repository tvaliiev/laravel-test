@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12">
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        @php
        $route = 'posts.store';
        if(isset($post)) {
            $route = ['posts.update', $post->id];
        } else {
            $post = (object) array(
                'title' => '',
                'content' => '',
            );
        }
        @endphp
        {{Form::open(['route' => $route, 'files' => true])}}
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            {{Form::label('title', 'Title',['class' => 'control-label', 'required' => true ])}}
                            {{Form::input('text', 'title', $request->old('title', $post->title), ['class' => 'form-control'])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            {{Form::label('content', 'Content',['class' => 'control-label'])}}
                            {{Form::textarea('content' , $request->old('content', $post->content), ['class' => 'form-control', 'required' => true ])}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="input-group">
                            {{Form::label('image', 'Preview',['class' => 'control-label'])}}
                            {{Form::file('image')}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <br>
                        {{Form::submit('Save', ['class' => 'btn btn-success'])}}
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-12">
                        @php
                            $old_categories = $request->old('categories');
                        @endphp
                        <select multiple="multiple" id="my-select" name="categories[]">
                            @foreach($categories as $category)
                                <option
                                        value="{{$category->id}}"
                                        @php
                                        $found = false;
                                        if(isset($old_categories)) {
                                            if(in_array($category->id, $old_categories))
                                                $found = true;
                                        } elseif(isset($post_categories)) {
                                            foreach($post_categories as $e) {
                                                if($e->id == $category->id) {
                                                    $found = true;
                                                    break;
                                                }
                                            }
                                        }
                                        if ($found) echo 'selected';
                                    @endphp
                            >{{$category->name}}</option>
                        @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p style="margin-top: 20px">Apply additional categories (separated by comma):</p>
                        <input type="text" name="new_categories" value="{{$request->old('new_categories')}}">
                    </div>
                </div>
            </div>
        </div>
        {{Form::close()}}
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('#my-select').multiSelect({
            selectableHeader: "<div class='text-center' style='background-color: #ececec; border-radius: 5px 5px 0 0;'>Available categories</div>",
            selectionHeader: "<div class='text-center' style='background-color: #ececec; border-radius: 5px 5px 0 0;'>Applied categories</div>",
        });
    </script>
@endpush