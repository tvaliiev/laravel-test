<?php

namespace App\Http\Controllers;

use App\Post;
use App\PostCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $category_id = $request->query('category', '');
        if($category_id != '') {
            $posts = Post::category($category_id)->orderBy('created_at', 'desc');
        } else {
            $posts = Post::orderBy('created_at', 'desc');
        }

        $posts = $posts->paginate(5)
            ->appends($request->except('page'));
        return view('posts.list', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = PostCategory::all();
        return view('posts.create', compact('categories', 'request'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'required|min:5',
            'content' => 'required|min:20',
        ]);

        $post = new Post();

        if($request->hasFile('image')) {
            $image = $request->file('image');
            $post->handleImage($image);
        }

        $post->user_id = Auth::user()->id;
        $post->fill($request->input());
        $post->save();

        $category_ids = $request->input('categories');
        $post->categories()->attach($category_ids);

        $new_categories = explode(",", $request->input('new_categories', ''));
        foreach ($new_categories as $new_category) {
            $post_category = new PostCategory();
            $post_category->name = trim($new_category);
            if($post_category->name !== '') {
                $post_category->save();
                $post_category->posts()->attach($post->id);
            }
        }


        return redirect(route('posts.show', ['id' => $post->id]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
       return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post, Request $request)
    {
        $categories = PostCategory::all();
        $post_categories = $post->categories()->get();
        return view('posts.create', compact('post', 'categories', 'post_categories', 'request'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $this->validate($request, [
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'title' => 'required|min:5',
            'content' => 'required|min:20',
        ]);

        if($request->hasFile('image')) {
            $imageFile = $request->file('image');
            $post->handleImage($imageFile);
        }

        $post->user_id = Auth::user()->id;
        $post->fill($request->input());
        $post->save();

        $category_ids = $request->input('categories');
        $post->categories()->detach($category_ids);
        $post->categories()->attach($category_ids);

        $new_categories = explode(",", $request->input('new_categories', ''));
        foreach ($new_categories as $new_category) {
            $post_category = new PostCategory();
            $post_category->name = trim($new_category);
            if($post_category->name !== '') {
                $post_category->save();
                $post_category->posts()->attach($post->id);
            }
        }

        return redirect(route('posts.show', ['id' => $post->id]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect(route('posts.list'));
    }
}
